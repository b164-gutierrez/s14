
//Staments, Syntax and Comments

//JS statements usually ends with semicolon(;), are not required 
//but we will use it to help us train to locate the end of statement.

console.log("Hello World");

// Note: convention: Breaking up long statements into multiple lines is common paractice.

/*
Multiple Comments
*/

// Variables = Used to contain data (like data memory)

// Decalre Varaibles
// Syntax: let/const variableName; var

// let,con,var = keyword used in declaring a variable.

// let myVariable;
// console.log(myVariable)

const myVariable = "Hello Again"; 
console.log(myVariable)

/*Guides in writing variables:
10 we can use 'let,const,var' keyword followed by the variable name 
your choosing and use the assignment operator(=) to assign value.
2. Variable names should tart with a lowercase character, use camelCase
for multiple words.*/

/*Best practices in naming variables:
1.When naming variables, it is important to creat variables that are
descriptive and indicative of tha data it contains.*/

let firstName = "Jane" // good variable name
let lastName = "Doe"
let pokemon = "2500" //bad variable name

/*2. It is better to start with lower case letter. Because there are several 
keywords in JS that start in capital letter.
3. Do not add spaces to your variable names.*/

/*sample: a. let first name = "Mike", (wrong)
		b. let product_description = "sample; (right; use underscore or camelCase)
		*/

// Declaring and initializing variables
/*Initializing varibales = the instance wher a variable
 is given its initial / starting value*/

 // Syntax: let,const variableName = vlue;

 let productName = "desktop computer"
 console.log(productName)

 let productPrice = 1899;
 console.log(productPrice)

 const	interest = 3.539

 // Reassigning variable values
productName = "Laptop"
console.log(productName)

// let variable can be reassigned with another value;
// let variable cannot be re-declared within its scope;

let friend = "Kate";
let anotherFriend = "John";

// const variable = fixed / constant value.
// error will show if if change. 

const pi = "3.14";


// Reassigning variable vs Initializing variables
// let - we can declare a variable first.

let supplier;
// Initialization is done after the variable has been declared

supplier = "John Smith Tradings";
console.log(supplier);

// reassign it's initial value
supplier = "Zuitts Store";
console.log(supplier)

/*const variables are variables woth constant data. Therefore we
should not re-declare, re-assign or even declare const variable without initialization.*/

// var vs. let,const
/*var - is also used in declaring a variable. vac is an ECMA script (ES1) feature
ES1 (JS 1997).*

/*let & cons was introduced as a new feature in ES6 (2015)*/

//difference of var and let,const?
/*Ther are issues when we used var in declaring variables,
1.	Hoisting = JS default behavior of moving declarations to the top
2. Scope = essentially means where these variables are available to use*/

// let,const can be used as a local/gobal scope.

//global scope
const outerVariable = "hello"; 


//block,local scope = {}
{
	const innerVariable = "hello aga1in"; 
	console.log(innerVariable)
}

// console.log(innerVariable);



a = 5;
console.log(a);
var a;

// Multiple Variable declarations

let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

						// Data Types

/* 1. Strings = series of characters that create a word, phrase, 
a sentence or anything related to creating text.
	a. in JS can be wrtten using either a single ('') or double ("") quotation.
*/
let country = 'Phippines'
let province = "Metro Manila"
console.log(typeof country)
console.log(typeof province)

/*Concatenating Strings
	a. multiple strings values can be combined to creat a single string using
	the "+" symbol*/

let fullAddress = province + ',' + country;
console.log(fullAddress)

let greeting = 'I live in the ' + country;
console.log(greeting)

// Template literals (ES6) is the updated version of concatenation
// backticks `dvdv`
// expression ${}
console.log(`I live in the ${province}, ${country}`);


let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// \'

let message = "John\"s employees went home early";
console.log(message);


message = "John's employees went home early"
console.log(message);

console.log(
`Metro Manila

"Hello"

'Hello'

Philippines`
	)

/*Numbers*/
/*Integers,Whole Numbers*/

let headCount = 23;
console.log(typeof headCount);

// Decimal Numbers,Fractions

let grade = 98.7;
console.log(typeof grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(typeof planetDistance);

// Combining variable with number data type and sting

console.log("John's grade last quarter" + grade);
console.log(`John's grade last quarter ${grade}`);

					// Boolean value = true/false

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

// Objects

						/*Arrays*/

// similar data types
// syntax: let/const arrayName = [elementA, elementB, etc]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
console.log(typeof grades);

				// Different data types
// storing diff data types inside an array
// not recommended
// make no sense to join context of programming

let details = [ "John", "Smith", 32, true];
console.log(details);

// Indexing
console.log(grades[1], grades[2]);

				// Reassigning array elements

/*let anime = ['one pice','one punch man', 'attack on titan'];
console.log(anime);
anime[0] = 'kimtsu no yaiba'
console.log(anime);*/

const anime = ['one pice','one punch man', 'attack on titan'];
console.log(anime);

anime[0] = 'kimtsu no yaiba'
console.log(anime);

/* const keyword is a little misleading. 
it does not define a constant value. it defines a constant refrence to a value.*/

						// Objects
// Syntax
/*
let,const objectName = {propertyA: value, propertyB: value}

gives an individual piece of information and it is called a property of the object.

they are used to create complex data that contains pieces of 
information that area relevant to each other.
*/

let objectGrades = {
	firstQuarter:98.7, 
	secondQuarter:92.1, 
	thirdQuarter:90.2, 
	fourthQuarter:94.6
};
console.log(objectGrades);

let person = {
	fullName: 'Juan Dela Cruz',
	age:35,
	isMarried: false,
};
console.log(person.fullName + isMarried);
console.log(person.age);

// Mini Activity

let firstName2 = 'Bee-Ar';
let lastName2 = 'Gutierrez';
console.log(firstName2, lastName2);

let sentence = (`${firstName2} ${lastName2} a student in Zuitt`);
console.log(sentence);

const favoriteFood = ['buffalo wings','kare kare', 'dinakdakan'];
console.log(favoriteFood);

let webDev = {
	firstName3: 'Bee-Ar',
	lastName3: 'Gutierrez',
	isDeveloper: true,
	hasPortfolio: true,
	age: 35
};
console.log(webDev);

/*March 04, 2022 Discussion*/



						// Number
let numb1 = 5;
let numb2 = 6;
let numb3 = 5.5;
let numb4 = .5;

let numString1 = '5';
let numString2 = '6';
console.log(numString1 + numString2); //"56" strings were concatenated.
console.log(numb1 + numb2);

		// Type Coercion/Forced Coercion
		/*
		   is the automatic or implicit conversion of values from one data
		   type to another.
		*/
console.log(numString1 + numb1); //"55" resulted in concatenated.


// Modulo %
console.log(numb1 % numb2); //(5/6) = remainder 5
console.log(numb2 % numb1); //(6/5) = remainder 1

console.log(numb2 - numb1);

let product = 'The number is' + numb1 * numb2;
console.log(product)

						// Null

let girlFriend = null;
console.log(girlFriend);

let myNumber = 0;
let myString = '';
console.log(myNumber);
console.log(myString);
// Sames as 0 value and empty string. For readability purposed.

						// Undefined
// Represents the state of a variable that has been decalred but without an assigned value.

let sampleVariable;
console.log(sampleVariable);

/*Difference between undefined and null is that for undefined, 
a variable was created but was not provied a value.*/

/*Null means that a variable was created and was assigned a
value taht does not hold any value/amount.
*/


							// Function	
/*in JS, it is a line/s/blokc of codes that 
tells our app to perform a certain task when called/invoked.*/

// Function declaration
/*defines a function with the function and specified parameters*/

/*
Syntax:
	functin functionName() {
	
	code block/statements. the block of code will be executed
	once the function has been run/called/invoked.

	}


	1. function keyword => defined a JS function.
	2. function name => camelCase to name our function.
	3. function block/statement => the statements which comprise the
	body of the funtion. this is where the code to be executed.

*/

// Sample 1:

// function printName(){
// 	console.log("My name is John");
// };


					//Function Expression
/*
a function expression can be stored in a variable.
*/ 

			// Anonymous function => function without a name.
// sample 2
let variableFunction = function(){
	console.log("Hello Again")
};

// sample 3
let funcExpression = function func_name(){
	console.log("Hello!")
};


				// Function Invocation
/*
the code inside a function is executed 
when the function is called/invoked.
*/


// Let's invoke/call the function that we declared.

// printName();

function declaredFunction() {
	console.log("Hello World")
};

declaredFunction();

// How about the function expression?
/*
They are always invoked/called using the variable name.
*/

variableFunction();

// Function scoping
// Global and local scope

// Functions scoped
// let faveChar = "Nezuko"; //Global scope

function myFunction(){
	let nickName = "Jane"; //local scope {}
	console.log(nickName);
	// console.log(faveChar);
}

myFunction();
// console.log(faveChar);
// console.log(nickName); //not defined

// Variables defined inside a function are not accesible/visible from outside the function
// var,let,const are quite similar when declared inside a function
// variables declared globally (outside of the function) have a global scope or it can access anywhere.


function showSum(){
	console.log(6 + 6)
}

showSum();

					// Parameters and Arguments
// "name" is called a parameter
/* parameter => acts a name variable/container that exits only inside of the function. 
usually parameter is the placeholder of an value.*/

let name = "Jane Smith";

function printName(pika){
	console.log(`My name is ${pika}`);
};

// argument is the actual value/data that we passed to our function.
// we can also use an variable as an argument

printName(name);

							// Multiple Parameters and Arguments

function displayFullName(fName, lName, age){
	console.log(`${fName}, ${lName}, is ${age},`)
}

displayFullName("Judy", "Medalla", "Jane", 60);
// providing less arguments that the expected parameters will automatically assign an undefined value to the parameter
// providing more arguments will not affect our function.


// Return keyword
/*
a. the "return" statement allows the output of a function of a to be passed 
	to the block of code that invoked the function.
b. any line/block of code that comes after function may return a statement is ignored
	because it ends the function execution.
c. return  keyword is used so that a function may return a value.
d. after returning the value, the next statement will stop its process.
e. result of this function (return) can be saved into a variable.
*/

function createFullName(firstName, middleName, lastName){
	return `${firstName} ${middleName} ${lastName}`
	console.log("I will no longer run because the function's value/result has been returned")
}

let fullName1 = createFullName("Tom,", "Cruies", "Mapother");
console.log(fullName1);

let fullName2 = displayFullName("William", "Bradley", "Pitt")
console.log(fullName2);

let fullName3 = createFullName("Jeffrey", "John", "Smith");
console.log(fullName3);



				// Mini Activity



function createNumber(num1, num2){
	return num1 / num2
	// console.log("")
}

let quotient = createNumber(8, 4);
console.log(`The result of the division is: ${quotient}`);

// prompt("Please Enter your second number:")

		
	
function argumentFunction(){
	console.log("This fucntion was passed as an argument before the message was printed")
}

function invokeFunction(pikachu){
	pikachu();
}

invokeFunction(argumentFunction)

